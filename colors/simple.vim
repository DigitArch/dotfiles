" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file

" colors:
" black = 16
" darkgrey = 239
" coolgrey = 103
" light grey = 102
" lighter grey = 188
" white = 231

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "simple"
hi clear Normal		
hi clear Comment	
hi clear Constant	
hi clear Special	
hi clear Identifier 
hi clear Statement	
hi clear Keyword	
hi clear PreProc	
hi clear Type		
hi clear Function	
hi clear Repeat		
hi clear Operator	
hi clear Ignore		
hi clear Error		
hi clear ErrorMsg
hi clear Todo	 	
hi clear Underlined
hi clear Directory
hi clear Question
hi clear MatchParen
hi Normal		cterm=none 			ctermfg=188
hi Comment		cterm=none			ctermfg=102
hi Constant		cterm=italic		ctermfg=188
hi Special		cterm=bold			ctermfg=188
hi Identifier 	cterm=bold			ctermfg=188
hi Statement	cterm=none			ctermfg=188
hi Keyword		cterm=none			ctermfg=188
hi PreProc		cterm=italic,bold	ctermfg=102
hi Type			cterm=bold			ctermfg=188
hi Function		cterm=bold			ctermfg=188
hi Repeat		cterm=bold			ctermfg=188
hi Operator		cterm=bold			ctermfg=188
hi Ignore		cterm=none			ctermfg=239
hi Error		cterm=bold			ctermfg=102
hi Todo	 		cterm=italic		ctermfg=188
hi Underlined 	cterm=underline		ctermfg=188
hi Directory	cterm=italic,bold	ctermfg=231
hi MatchParen	cterm=bold			ctermfg=231
hi link Question Normal
hi link ErrorMsg Error

"spelling
hi clear SpellBad
hi clear SpellCap
hi clear SpellRare
hi clear SpellLocal
hi SpellBad		cterm=underline 	ctermfg=102
hi link SpellCap SpellBad
hi link SpellRare Normal
hi link SpellLocal Normal

"vim interface colors
hi clear StatusLine
hi clear StatusLineNC
hi clear StatusLineTerm
hi clear StatusLineTermNC
hi clear VertSplit
hi clear Folded
hi clear Search
hi clear IncSearch
hi clear Cursor
hi clear iCursor
hi clear LineNr
hi clear CursorLineNr
hi clear CursorLine
hi clear Title
hi clear Pmenu
hi StatusLine	cterm=bold			ctermfg=188		ctermbg=239
hi Folded		cterm=bold 			ctermfg=188 	ctermbg=239
hi Search 		cterm=bold,italic	ctermfg=188
hi Cursor		cterm=none			ctermfg=16 		ctermbg=231
hi iCursor		cterm=none			ctermfg=16 		ctermbg=231
hi LineNr 		cterm=none			ctermfg=102
hi CursorLineNr cterm=bold			ctermfg=102
hi Pmenu		cterm=none 			ctermfg=16		ctermbg=103
hi link StatusLineNC StatusLine
hi link StatusLineTerm StatusLine
hi link StatusLineTermNC StatusLine
hi link VertSplit StatusLine
hi link IncSearch Search
hi link Title Special

"Diffs
hi clear DiffAdd
hi clear DiffChange
hi clear DiffDelete
hi clear DiffText
hi DiffAdd 		cterm=bold			ctermfg=188
hi DiffChange	cterm=italic		ctermfg=188
hi DiffDelete	cterm=none			ctermfg=102
hi link DiffText Normal

"vim syntax
hi clear vimCtrlChar
hi vimCtrlChar	cterm=italic 		ctermfg=188

" Markdown Groups
hi clear MarkdownHeading
hi clear MarkdownHeadingDelimiter
hi clear htmlspecialChar
hi clear markdownAutomaticLink
hi clear markdownBlockquote
hi clear markdownBold
hi clear markdownIdDeclaration
hi clear markdownItalic
hi clear markdownLinkText
hi clear markdownListMarker
hi clear markdownUrl
hi clear markdownUrlTitle
hi MarkdownHeading cterm=bold ctermfg=231
hi MarkdownHeadingDelimiter cterm=bold ctermfg=231
hi htmlspecialChar ctermfg=188
hi markdownAutomaticLink cterm=underline
hi markdownBlockquote cterm=italic ctermfg=188
hi markdownBold cterm=bold
hi markdownIdDeclaration cterm=bold ctermfg=231
hi markdownItalic cterm=italic
hi markdownLinkText cterm=underline
hi markdownListMarker cterm=bold
hi markdownUrl cterm=underline ctermfg=188
hi markdownUrlTitle cterm=underline,bold
hi link markdownH1 MarkdownHeading
hi link markdownH2 MarkdownHeading
hi link markdownH3 MarkdownHeading
hi link markdownH4 MarkdownHeading
hi link markdownH5 MarkdownHeading
hi link markdownH6 MarkdownHeading

" Common groups that link to default highlighting.
" You can specify other highlighting easily.

hi link String	Constant
hi link Character	Constant
hi link Number	Constant
hi link Boolean	Constant
hi link Float		Number
hi link Conditional	Repeat
hi link Label		Statement
hi link Exception	Statement
hi link Include	PreProc
hi link Define	PreProc
hi link Macro		PreProc
hi link PreCondit	PreProc
hi link StorageClass	Type
hi link Structure	Type
hi link Typedef	Type
hi link Tag		Special
hi link SpecialChar	Special
hi link Delimiter	Special
hi link SpecialComment Special
hi link Debug		Special

