" General settings:
set autoindent
set backspace=indent,eol,start
set breakindent
set copyindent
set cursorline
set hlsearch
set ignorecase
set linebreak
set loadplugins
set nocompatible
set noerrorbells
set number
set shiftround
set shiftwidth=4
set showmatch
set smartcase
set splitbelow
set splitright
set t_ZH=[3m
set t_ZR=[23m
set t_vb=
set tabstop=4 "Python tabstob
set visualbell
set wildignore=*.aux,*.log,*.bak,*.pyc
colorscheme simple
filetype plugin on
nohlsearch
syntax on

let mapleader = " "

"Normal maps
nnoremap w W
nnoremap W w
nnoremap b B
nnoremap B b
nnoremap e E
nnoremap E e
nnoremap ; :
nnoremap : ;
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <Leader><tab> :tabnext<CR>
nnoremap <Leader><S-tab> :tabprevious<CR>
nnoremap <C-S-T> :tabnew<CR>
nnoremap <Leader>u g~l
nnoremap <Leader>j ddp
nnoremap <Leader>k ddkP
nnoremap <Leader>. >>
nnoremap <Leader>, <<
nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc>
nnoremap <Leader>c :.!clip.exe<CR>u
nnoremap <Leader>C :.!clip.exe<CR>
nnoremap <Leader>u guu<CR>
nnoremap <Leader>U gUU<CR>
nnoremap <Up> gk
nnoremap <Down> gj
nnoremap <Home> g<Home>
nnoremap <End> g<End>

"Operator Pending Maps:
onoremap w W
onoremap W w
onoremap b B
onoremap B b
onoremap e E
onoremap E e

"Visual Maps:
vnoremap w W
vnoremap W w
vnoremap b B
vnoremap B b
vnoremap e E
vnoremap E e
vnoremap ; :
vnoremap : ;
vnoremap <Down> gj
vnoremap <Up> gk
vnoremap <Home> g<Home>
vnoremap <End> g<End>
vnoremap <Leader>. >>
vnoremap <Leader>, <<
vnoremap <Leader>c :'<,'>!clip.exe<CR>u
vnoremap <Leader>C :'<,'>!clip.exe<CR>


"Insert Maps:
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk
inoremap <Home> <C-o>g<Home>
inoremap <End> <C-o>g<End>
inoremap <C-J> <Esc><C-W><C-J>
inoremap <C-K> <Esc><C-W><C-K>
inoremap <C-L> <Esc><C-W><C-L>
inoremap <C-H> <Esc><C-W><C-H>

"F Keys
map <F1> ;w !wc<CR>
map <F2> ;nohlsearch<CR>
map <F3> ;set spell!<CR>
map <F4> ;set wrap!<CR>
map <F5> ;set paste!<CR>
map <F6> ;set number!<CR>
map <F10> ;if exists("g:syntax_on") <Bar>
	\   syntax off <Bar>
	\ else <Bar>
	\   syntax on <Bar>
	\ endif <CR>	

"Functions:
command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis 
command EmailCopy %! pandoc -t html - | clip.exe && powershell.exe 'get-clipboard | set-clipboard -ashtml'

"Local vimrc
if filereadable(glob("~/.vimrc.local"))
	source ~/.vimrc.local
endif
